.. _gpencil_weight_paint-toolbar-index:

******************
Weight Paint Tools
******************

For Grease Pencil Weight Paint modes each brush type is exposed as a tool,
the brush can be changed in the Tool Settings.
See :doc:`Brush </grease_pencil/modes/weight_paint/tool_settings/brush>` for more information.

.. figure:: /images/grease-pencil_modes_weight-paint_tools_brushes.png
   :align: right

Draw
   Paints a specified weight over the strokes.

Blur
   Smooths out the weighting of adjacent points. In this mode the Weight
   Value is ignored. The strength defines how much the smoothing is applied.

Average
   Smooths weights by painting the average resulting weight from all weights under the brush.

Smear
   Smudges weights by grabbing the weights under the brush and "dragging" them.
   This can be imagined as a finger painting tool.

:ref:`Annotate <tool-annotate-freehand>`
   Draw free-hand annotation.

   :ref:`Annotate Line <tool-annotate-line>`
      Draw straight line annotation.
   :ref:`Annotate Polygon <tool-annotate-polygon>`
      Draw a polygon annotation.
   :ref:`Annotate Eraser <tool-annotate-eraser>`
      Erase previous drawn annotations.
