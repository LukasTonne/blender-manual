
**************
Brush Settings
**************

.. figure:: /images/sculpt-paint_sculpting_tool-settings_brush-settings_brush-panel.png
   :align: right

   Brush settings panel.


Information on brush settings for every mode can be found in these pages:

:doc:`Brush </sculpt_paint/brush/brush_settings>`
   General and advanced settings.

:doc:`Texture </sculpt_paint/brush/texture>`
   Color and mask texture settings.

:doc:`Stroke </sculpt_paint/brush/stroke>`
   Stroke methods and settings.

:doc:`Falloff </sculpt_paint/brush/falloff>`
   Falloff curves and settings.

:doc:`Cursor </sculpt_paint/brush/cursor>`
   Cursor and appearance settings.

.. toctree::
   :hidden:

   Brush Settings </sculpt_paint/brush/brush_settings.rst>
   Texture </sculpt_paint/brush/texture.rst>
   Stroke </sculpt_paint/brush/stroke.rst>
   Falloff </sculpt_paint/brush/falloff.rst>
   Cursor </sculpt_paint/brush/cursor.rst>